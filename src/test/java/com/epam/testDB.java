package com.epam;

import com.epam.jdbc.DAO;
import com.epam.util.ListMatcher;
import com.epam.util.MapMatcher;
import org.junit.*;

import java.util.*;

import static org.junit.Assert.*;

public class testDB {

	private static final String SONG_NAME_TO_TEST_CALL = "BYOB";

	private static DAO dao = new DAO();

	@BeforeClass
	public static void initDB() {
		dao.createDB();
		dao.createSchema();
		dao.fillDataDB();
		dao.registerFunctions();
		dao.createTrigger();
	}

	@AfterClass
	public static void removeDB() {
		dao.dropFunctions();
		dao.dropDB();
	}

	@Test
	public void testCallableStatement() {
		assertEquals("System of a down", dao.findBandBySong(SONG_NAME_TO_TEST_CALL));
	}

	@Test
	public void testRead() {
		MapMatcher<String, Integer> matcher = new MapMatcher<>();
		Map<String, Integer> mapToMatch = new HashMap<>();
		mapToMatch.put("Youthanasia", 3);

		Map<String, Integer> mapReturned = dao.showAlbumsNumberSongsOver("Megadeth", 2);
		assertTrue(matcher.isMapsEquals(mapReturned, mapToMatch));
	}

	@Test
	public void testUpdate() {
		dao.getSongsOfBand("Metallica").forEach(song -> assertNotEquals("test", song));
		assertEquals("10 rows updated", dao.renameSongsOfBand("Metallica", "test"));
		dao.getSongsOfBand("Metallica").forEach(song -> assertEquals("test", song));
	}

	@Test
	public void testDelete() {
		ListMatcher<String> matcher = new ListMatcher<>();
		List<String> listToMatch = Arrays.asList("7 rows deleted", "2 rows deleted", "1 rows deleted");

		List<String> returnedList = dao.deleteAllAboutBand("Slayer");
		assertTrue(matcher.isListsMatch(returnedList, listToMatch));
	}
}
