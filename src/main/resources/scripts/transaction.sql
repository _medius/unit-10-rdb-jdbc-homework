DELETE FROM songs s
USING albums
WHERE s.album_id IN
	(SELECT a.id FROM albums a
	JOIN bands b
	ON b.id = a.band_id
	WHERE b.title = ? );

DELETE FROM albums a
USING bands b
WHERE a.band_id =
	(SELECT id FROM bands
	WHERE title = ? );

DELETE FROM bands
WHERE title = ? ;
