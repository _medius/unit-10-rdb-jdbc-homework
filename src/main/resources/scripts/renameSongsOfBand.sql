UPDATE songs s
SET title = ?
FROM albums
WHERE s.album_id IN
	(SELECT a.id FROM albums a
	JOIN bands b
	ON b.id = a.band_id
	WHERE b.title = ? );