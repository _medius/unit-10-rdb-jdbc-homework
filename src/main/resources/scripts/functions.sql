CREATE OR REPLACE FUNCTION find_band_by_song(band_title VARCHAR(255)) RETURNS VARCHAR AS
$$
SELECT b.title FROM bands b
INNER JOIN albums a
ON b.id = a.band_id
WHERE a.id =
    (SELECT s.album_id FROM songs s
    WHERE s.title = band_title);
$$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION forbid_more_than() RETURNS trigger
LANGUAGE plpgsql AS
 $$
DECLARE
	n bigint :=TG_ARGV[0];
BEGIN
	IF(SELECT COUNT(*) FROM deleted_rows) <= n IS NOT TRUE
	THEN
		RAISE EXCEPTION 'More than % rows deleted', n;
	END IF;
	RETURN OLD;
END;$$;
