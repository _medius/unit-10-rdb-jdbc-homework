SELECT a.title, COUNT(s.album_id) AS songs_number
FROM songs s
FULL OUTER JOIN albums a
ON a.id = s.album_id
WHERE a.band_id =
	(SELECT b.id FROM bands b
	WHERE b.title = ? )
GROUP BY a.title
HAVING COUNT(s.album_id) > ?;
