INSERT INTO bands (title, genre, members_number)
VALUES ('Metallica', 'Thrash metal', 4);
INSERT INTO bands (title, genre, members_number)
VALUES ('Slayer', 'Thrash metal', 4);
INSERT INTO bands (title, genre, members_number)
VALUES ('Megadeth', 'Thrash metal', 4);
INSERT INTO bands (title, genre, members_number)
VALUES ('System of a down', 'Alternative', 4);
INSERT INTO bands (title, genre, members_number)
VALUES ('Slipknot', 'Alternative', 9);

INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Iowa', 'Roadrunner records', '2001-08-28', 5);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Slipknot', 'Indigo Ranch Studios', '1999-06-29', 5);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Toxicity', 'Cello Studios', '2001-09-04', 4);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Mezmerize', 'AMPSR', '2005-05-17', 4);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Rust in Peace', 'Capitol Records', '1990-09-24', 3);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Youthanasia', 'Capitol Records', '1994-10-01', 3);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Divine Intervention', 'Sound City', '1994-09-27', 2);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Seasons in the Abyss', 'Def American Records', '1990-10-09', 2);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Master of puppets', 'Sweet Silence Studios', '1986-03-03', 1);
INSERT INTO albums (title, music_lable, release_date, band_id)
VALUES ('Black Album', 'One on One Recording', '1991-08-12', 1);

INSERT INTO songs (title, album_id)
VALUES ('My Plague', 1);
INSERT INTO songs (title, album_id)
VALUES ('Gently', 1);
INSERT INTO songs (title, album_id)
VALUES ('Left Behind', 1);
INSERT INTO songs (title, album_id)
VALUES ('Eyeless', 2);
INSERT INTO songs (title, album_id)
VALUES ('Wait and bleed', 2);
INSERT INTO songs (title, album_id)
VALUES ('Spit it out', 2);
INSERT INTO songs (title, album_id)
VALUES ('Liberate', 2);
INSERT INTO songs (title, album_id)
VALUES ('Jet pilot', 3);
INSERT INTO songs (title, album_id)
VALUES ('Prison song', 3);
INSERT INTO songs (title, album_id)
VALUES ('Tixicity', 3);
INSERT INTO songs (title, album_id)
VALUES ('Chop suey!', 3);
INSERT INTO songs (title, album_id)
VALUES ('BYOB', 4);
INSERT INTO songs (title, album_id)
VALUES ('Revenga', 4);
INSERT INTO songs (title, album_id)
VALUES ('Sad statue', 4);
INSERT INTO songs (title, album_id)
VALUES ('Lucretia', 5);
INSERT INTO songs (title, album_id)
VALUES ('Hangar 18', 5);
INSERT INTO songs (title, album_id)
VALUES ('Youthanasia', 6);
INSERT INTO songs (title, album_id)
VALUES ('Reckoning Day', 6);
INSERT INTO songs (title, album_id)
VALUES ('Victory', 6);
INSERT INTO songs (title, album_id)
VALUES ('213', 7);
INSERT INTO songs (title, album_id)
VALUES ('Dittohead', 7);
INSERT INTO songs (title, album_id)
VALUES ('Fictional Reality', 7);
INSERT INTO songs (title, album_id)
VALUES ('Hallowed Point', 8);
INSERT INTO songs (title, album_id)
VALUES ('Blood Red', 8);
INSERT INTO songs (title, album_id)
VALUES ('Temptation', 8);
INSERT INTO songs (title, album_id)
VALUES ('Spirit in Black', 8);
INSERT INTO songs (title, album_id)
VALUES ('Battery', 9);
INSERT INTO songs (title, album_id)
VALUES ('Master of Puppets', 9);
INSERT INTO songs (title, album_id)
VALUES ('Welcome Home', 9);
INSERT INTO songs (title, album_id)
VALUES ('Disposable Heroes', 9);
INSERT INTO songs (title, album_id)
VALUES ('Orion', 9);
INSERT INTO songs (title, album_id)
VALUES ('Enter Sandman', 10);
INSERT INTO songs (title, album_id)
VALUES ('Sad but True', 10);
INSERT INTO songs (title, album_id)
VALUES ('The Unforgiven', 10);
INSERT INTO songs (title, album_id)
VALUES ('Wherever I May Roam', 10);
INSERT INTO songs (title, album_id)
VALUES ('Nothing Else Matters', 10);
