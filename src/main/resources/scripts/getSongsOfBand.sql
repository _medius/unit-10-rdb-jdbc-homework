SELECT s.title FROM songs s
JOIN albums a
ON s.album_id = a.id
WHERE s.album_id IN
	(SELECT a.id FROM albums a
	JOIN bands b
	ON b.id = a.band_id
	WHERE b.title = ?);