DROP TABLE IF EXISTS songs;
DROP TABLE IF EXISTS albums;
DROP TABLE IF EXISTS bands;

CREATE TABLE bands (
    id SERIAL NOT NULL,
    genre VARCHAR(255),
    members_number int4,
    title VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE albums (
    id SERIAL NOT NULL,
    music_lable VARCHAR(255),
    release_date DATE,
    title VARCHAR(255),
    band_id INT4 NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (band_id) references bands (id)
);

CREATE TABLE songs (
    id SERIAL NOT NULL,
    title VARCHAR(255),
    album_id INT4 NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (album_id) references albums (id)
);
