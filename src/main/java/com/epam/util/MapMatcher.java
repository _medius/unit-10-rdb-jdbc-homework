package com.epam.util;

import java.util.Map;
import java.util.Set;

public class MapMatcher<K, V> {

	public boolean isMapsEquals(Map<K, V> firstMap, Map<K, V> secondMap) {
		if (firstMap.size() != secondMap.size()) {
			return false;
		}

		if (firstMap.size() == 0) {
			return true;
		}

		Set<Map.Entry<K, V>> firstSet = firstMap.entrySet();
		for (Map.Entry<K, V> entry : firstSet) {
			if (secondMap.get(entry.getKey()) == null) {
				return false;
			}

			if (!entry.getValue().equals(secondMap.get(entry.getKey()))) {
				return false;
			}
		}

		return true;
	}
}
