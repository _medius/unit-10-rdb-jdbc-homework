package com.epam.util;

import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ScriptReader {

	private static final Logger LOG = Logger.getLogger(ScriptReader.class);

	public static List<String> readScript(String path) {
		List<String> commands = new ArrayList<>();

		try (BufferedReader reader = new BufferedReader(new FileReader(new File(path)))) {
			StringBuilder builder = new StringBuilder();
			boolean isFunction = false;
			String line;

			while ((line = reader.readLine()) != null) {
				builder.append(line).append(" ");

				if (line.contains("$$")) {
					isFunction = true;
				}

				LOG.trace(line);

				if (!isFunction && (line.endsWith(";") || line.endsWith("}"))) {
					commands.add(builder.toString());
					builder.setLength(0);
				}

				if (isFunction && line.contains("$$") && line.contains(";")) {
					commands.add(builder.toString());
					builder.setLength(0);
					isFunction = false;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return commands;
	}
}
