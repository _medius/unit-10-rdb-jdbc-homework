package com.epam.jdbc;

import com.epam.util.ScriptReader;
import org.apache.log4j.Logger;

import java.io.*;
import java.sql.*;
import java.util.*;

public class JDBCconnection {

	private static final Logger LOG = Logger.getLogger(JDBCconnection.class);

	private static final Properties PROPERTIES = new Properties();

	private static final String DRIVER_PROPERTY_KEY = "driver";
	private static final String URL_PROPERTY_KEY = "url";
	private static final String USERNAME_PROPERTY_KEY = "username";
	private static final String PASSWORD_PROPERTY_KEY = "password";
	private static final String DB_NAME_PROPERTY_KEY = "db_name";

	{
		loadProperties();
		readProperties();
		registerDriver();
	}

	private String driver;
	private String urlServer;
	private String urlDB;
	private String username;
	private String password;
	private String dbName;

	private void registerDriver() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	private void loadProperties() {
		try {
			PROPERTIES.load(new FileReader(System.getProperty("user.dir")
					+ "\\src\\main\\resources\\db.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void readProperties() {
		driver = PROPERTIES.getProperty(DRIVER_PROPERTY_KEY);
		urlServer = PROPERTIES.getProperty(URL_PROPERTY_KEY);
		dbName = PROPERTIES.getProperty(DB_NAME_PROPERTY_KEY);
		urlDB = urlServer + dbName;
		username = PROPERTIES.getProperty(USERNAME_PROPERTY_KEY);
		password = PROPERTIES.getProperty(PASSWORD_PROPERTY_KEY);
	}

	public void updateDB(String path) {
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			try (Statement statement = connection.createStatement()) {
				List<String> commands = ScriptReader.readScript(path);
				commands.forEach(command -> {
					try {
						statement.executeUpdate(command);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public String findBandBySong(String path, String songTitle) {
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			String callProcedure = ScriptReader.readScript(path).get(0);
			try (CallableStatement statement = connection.prepareCall(callProcedure)) {
				statement.registerOutParameter(1, Types.VARCHAR);
				statement.setString(2, songTitle);
				statement.executeUpdate();
				String bandName = statement.getString(1);
				LOG.info("Callable statement. By song " + songTitle + " found group " + bandName + '.');
				return bandName;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}

	public Map<String, Integer> showAlbumsNumberSongsOver(String path, String bandTitle, int numberOfSongs) {
		Map<String, Integer> result = new HashMap<>();
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			String selectSQL = ScriptReader.readScript(path).get(0);
			try (PreparedStatement statement = connection.prepareStatement(selectSQL)) {
				statement.setString(1, bandTitle);
				statement.setInt(2, numberOfSongs);
				ResultSet resultSet = statement.executeQuery();
				ResultSetMetaData metaData = resultSet.getMetaData();
				LOG.info("Prepared statement. Albums of group " + bandTitle
						+ " with number of songs over than " + numberOfSongs);
				LOG.info(metaData.getColumnName(1) + " " + metaData.getColumnName(2));
				while (resultSet.next()) {
					LOG.info(resultSet.getString(1) + " " + resultSet.getInt(2));
					result.put(resultSet.getString(1), resultSet.getInt(2));
				}
				System.out.println();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public List<String> deleteAllAboutBand(String path, String bandTitle) {
		List<String> result = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			connection.setAutoCommit(false);

			List<String> commands = ScriptReader.readScript(path);
			LOG.info("Start transaction");
			commands.forEach(command -> {
				try (PreparedStatement statement = connection.prepareStatement(command)) {
					statement.setString(1, bandTitle);
					int deletedRows = statement.executeUpdate();
					String resultLine = deletedRows + " rows deleted";
					result.add(resultLine);
					LOG.info(resultLine);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			});

			connection.commit();
			LOG.info("Commit transaction");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public String renameSongsOfBand(String path, String nameOfBand, String songsNewName) {
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			String sql = ScriptReader.readScript(path).get(0);
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, songsNewName);
				statement.setString(2, nameOfBand);
				String result = statement.executeUpdate() + " rows updated";
				LOG.info("Rename all songs of group " + nameOfBand + " to name " + songsNewName + '.');
				LOG.info(result);
				return result;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return "";
	}

	public List<String> getSongsOfBand(String path, String nameOfBand) {
		List<String> result = new ArrayList<>();
		try (Connection connection = DriverManager.getConnection(urlDB, username, password)) {
			String sql = ScriptReader.readScript(path).get(0);
			try (PreparedStatement statement = connection.prepareStatement(sql)) {
				statement.setString(1, nameOfBand);
				ResultSet resultSet = statement.executeQuery();
				while (resultSet.next()) {
					result.add(resultSet.getString(1));
				}
				LOG.info("All songs of group " + nameOfBand + '.');
				LOG.info(resultSet.getMetaData().getColumnName(1));
				result.forEach(LOG::info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	public void converseDB(String path) {
		try (Connection connection = DriverManager.getConnection(urlServer, username, password)) {
			try (Statement statement = connection.createStatement()) {
				String command = String.format(ScriptReader.readScript(path).get(0), dbName);
				statement.executeUpdate(command);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
