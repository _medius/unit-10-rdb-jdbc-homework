package com.epam.jdbc;

import java.util.List;
import java.util.Map;

public class DAO {
	private static final String SCRIPTS_PACKAGE = System.getProperty("user.dir") + "\\src\\main\\resources\\scripts\\";

	private static final String CREATE_SCHEMA_SCRIPT = SCRIPTS_PACKAGE + "schema.sql";
	private static final String FILL_DATA_SCRIPT = SCRIPTS_PACKAGE + "testData.sql";
	private static final String TRANSACTION_SCRIPT = SCRIPTS_PACKAGE + "transaction.sql";
	private static final String TRIGGER_SCRIPT = SCRIPTS_PACKAGE + "trigger.sql";
	private static final String DROP_FUNCTIONS_SCRIPT = SCRIPTS_PACKAGE + "dropFunctions.sql";
	private static final String PROCEDURE_SCRIPT = SCRIPTS_PACKAGE + "functions.sql";
	private static final String QUERIES_SCRIPT = SCRIPTS_PACKAGE + "queries.sql";
	private static final String FIND_BAND_BY_SONG = SCRIPTS_PACKAGE + "callProcedure.sql";
	private static final String CREATE_DB_SCRIPT = SCRIPTS_PACKAGE + "createDB.sql";
	private static final String DROP_DB_SCRIPT = SCRIPTS_PACKAGE + "dropDB.sql";
	private static final String GET_SONGS_SCRIPT = SCRIPTS_PACKAGE + "getSongsOfBand.sql";
	private static final String RENAME_SONGS_SCRIPT = SCRIPTS_PACKAGE + "renameSongsOfBand.sql";

	private JDBCconnection jdbc = new JDBCconnection();

	/**
	 * Method deletes the database if it exists
	 * and creates new one with a name from the property file.
	 */
	public void createDB() {
		jdbc.converseDB(DROP_DB_SCRIPT);
		jdbc.converseDB(CREATE_DB_SCRIPT);
	}

	/**
	 * Method creates tables in the database.
	 */
	public void createSchema() {
		jdbc.updateDB(CREATE_SCHEMA_SCRIPT);
	}

	/**
	 * Method fills the database with test data.
	 */
	public void fillDataDB() {
		jdbc.updateDB(FILL_DATA_SCRIPT);
	}

	/**
	 * Method registers functions for testing callable
	 * statement and creating the trigger.
	 */
	public void registerFunctions() {
		jdbc.updateDB(PROCEDURE_SCRIPT);
	}

	/**
	 * Method creates trigger which forbids removal more
	 * than 7 rows from table songs.
	 */
	public void createTrigger() {
		jdbc.updateDB(TRIGGER_SCRIPT);
	}

	/**
	 * Method selects albums of the specified group with the number
	 * songs more than specified.
	 *
	 * @param bandTitle     - name of group
	 * @param numberOfSongs - number of songs
	 * @return - album names mapped to number of songs in it
	 */
	public Map<String, Integer> showAlbumsNumberSongsOver(String bandTitle, int numberOfSongs) {
		return jdbc.showAlbumsNumberSongsOver(QUERIES_SCRIPT, bandTitle, numberOfSongs);
	}

	/**
	 * Method finds the group to which the specified song belongs
	 *
	 * @param songTitle - name of song
	 * @return - group which belongs to the specified song
	 */
	public String findBandBySong(String songTitle) {
		return jdbc.findBandBySong(FIND_BAND_BY_SONG, songTitle);
	}

	/**
	 * Method deletes all rows in all tables about specified group
	 * in one transaction.
	 *
	 * @param bandTitle - group to remove
	 * @return - number of deleted rows in each operation
	 */
	public List<String> deleteAllAboutBand(String bandTitle) {
		return jdbc.deleteAllAboutBand(TRANSACTION_SCRIPT, bandTitle);
	}

	/**
	 * Method drops all functions and triggers created by this app.
	 */
	public void dropFunctions() {
		jdbc.updateDB(DROP_FUNCTIONS_SCRIPT);
	}

	/**
	 * Method renames all song which belongs to the specified group.
	 *
	 * @param nameOfBand   - name of group which songs need to rename
	 * @param songsNewName - new name of songs
	 * @return - number of updated rows
	 */
	public String renameSongsOfBand(String nameOfBand, String songsNewName) {
		return jdbc.renameSongsOfBand(RENAME_SONGS_SCRIPT, nameOfBand, songsNewName);
	}

	/**
	 * Method selects all songs which belongs to the specified group.
	 *
	 * @param nameOfBand
	 * @return
	 */
	public List<String> getSongsOfBand(String nameOfBand) {
		return jdbc.getSongsOfBand(GET_SONGS_SCRIPT, nameOfBand);
	}

	/**
	 * Method drops the database.
	 */
	public void dropDB() {
		jdbc.converseDB(DROP_DB_SCRIPT);
	}


}
