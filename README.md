# RDB - JDBC Homework
1. Package com.epam.jdbc contains a class for connecting to database 
called JDBCconnection and DAO object to work with it.
2. Package com.epam.util contains util classes to read sql scripts and
to compare collections by each element.
3. Parameters for connecting to the database stored in property file
db.properties which contains in src.main.resources. In directory 
resources.scripts stored sql scripts. Please specify url to the 
postgresql, password and username for connecting to database.
